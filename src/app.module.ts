import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { OauthUtilsModule } from './oauth-utils/oauth-utils.module';
import { AuthModule } from './auth/auth.module';
import { CacheModule } from '@nestjs/common/cache';

@Module({
  imports: [
    OauthUtilsModule,
    AuthModule,
    CacheModule.register({ ttl: 10, max: 100000, isGlobal: true }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
